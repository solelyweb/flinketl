/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.apache.flink.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Description: This class defines three running mode of Flinketl
 * <p>
 * @author: 894941211
 * <p>
 * @date: 2020/10/23
 */
public enum ClusterMode {

    /**
     * 本地模式运行
     */
    local(0, "local"),

    /**
     * flink集群 standalone模式
     */
    standalone(1, "standalone"),

    /**
     * 在已经启动在yarn上的flink session里上运行
     */
    yarn(2, "yarn"),

    /**
     * 在yarn上单独启动flink session运行
     */
    yarnPer(3, "yarnPer"),
    /**
     * 在yarn上单独启动flink application运行
     */
    application(4, "application");

    private int type;

    private String name;

    ClusterMode(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public static ClusterMode getByName(String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("ClusterMode name cannot be null or empty");
        }
        switch (name) {
            case "standalone":
                return standalone;
            case "yarn":
                return yarn;
            case "yarnPer":
                return yarnPer;
            case "application":
                return application;
            default:
                return local;
        }
    }

}

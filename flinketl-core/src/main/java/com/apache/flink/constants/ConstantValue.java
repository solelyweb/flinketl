/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.apache.flink.constants;

/**
 * @Description: ConstantValue
 * <p>
 * @author: 894941211
 * <p>
 * @date: 2020/10/23
 */
public class ConstantValue {

    public static final String STAR_SYMBOL = "*";
    public static final String POINT_SYMBOL = ".";
    public static final String EQUAL_SYMBOL = "=";
    public static final String SINGLE_QUOTE_MARK_SYMBOL = "'";
    public static final String DOUBLE_QUOTE_MARK_SYMBOL = "\"";
    public static final String COMMA_SYMBOL = ",";

    public static final String SINGLE_SLASH_SYMBOL = "/";
    public static final String DOUBLE_SLASH_SYMBOL = "//";

    public static final String LEFT_PARENTHESIS_SYMBOL = "(";
    public static final String RIGHT_PARENTHESIS_SYMBOL = ")";

    public static final String KEY_HTTP = "http";

    public static final String PROTOCOL_HTTP = "http://";
    public static final String PROTOCOL_HTTPS = "https://";
    public static final String PROTOCOL_HDFS = "hdfs://";
    public static final String PROTOCOL_JDBC_MYSQL = "jdbc:mysql://";

    public static final String SYSTEM_PROPERTIES_KEY_OS = "os.name";
    public static final String SYSTEM_PROPERTIES_KEY_USER_DIR = "user.dir";
    public static final String SYSTEM_PROPERTIES_KEY_JAVA_VENDOR = "java.vendor";
    public static final String SYSTEM_PROPERTIES_KEY_FILE_ENCODING = "file.encoding";

    public static final String OS_WINDOWS = "windows";

    public static final String TIME_SECOND_SUFFIX = "sss";
    public static final String TIME_MILLISECOND_SUFFIX = "SSS";

    public static final String FILE_SUFFIX_XML = ".xml";

    public static final int MAX_BATCH_SIZE = 200000;

    public static final long STORE_SIZE_G = 1024L * 1024 * 1024;

    public static final long STORE_SIZE_M = 1024L * 1024;

    public static final String SHIP_FILE_PLUGIN_LOAD_MODE = "shipfile";
    public static final String CLASS_PATH_PLUGIN_LOAD_MODE = "classpath";

    public static final String CLASSLOADER_CHILD_FIRST = "child-first";
    public static final String CLASSLOADER_PARENT_FIRST = "parent-first";

    public static final String CONFIG_FILE_LOGBACK_NAME = "logback.xml";
    public static final String CONFIG_FILE_LOG4J_NAME = "log4j.properties";

    public static final String KEYTAB_FILE_NAME = "krb5.keytab";

    public static final String ENV_APP_ID = "_APP_ID";
    public static final String ENV_CLIENT_HOME_DIR = "_CLIENT_HOME_DIR";
    public static final String ENV_CLIENT_SHIP_FILES = "_CLIENT_SHIP_FILES";

    public static final String ENV_FLINK_CLASSPATH = "_FLINK_CLASSPATH";

    public static final String FLINK_JAR_PATH = "_FLINK_JAR_PATH"; // the Flink jar resource location (in HDFS).
    public static final String FLINK_YARN_FILES = "_FLINK_YARN_FILES"; // the root directory for all yarn application files

    public static final String KEYTAB_PATH = "_KEYTAB_PATH";
    public static final String KEYTAB_PRINCIPAL = "_KEYTAB_PRINCIPAL";
    public static final String ENV_HADOOP_USER_NAME = "HADOOP_USER_NAME";
    public static final String ENV_ZOOKEEPER_NAMESPACE = "_ZOOKEEPER_NAMESPACE";

    public static final String ENV_KRB5_PATH = "_KRB5_PATH";
    public static final String ENV_YARN_SITE_XML_PATH = "_YARN_SITE_XML_PATH";

}

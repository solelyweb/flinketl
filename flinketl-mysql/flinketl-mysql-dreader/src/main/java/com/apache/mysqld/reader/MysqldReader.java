/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apache.mysqld.reader;

import com.apache.flink.config.DataTransferConfig;
import com.apache.flink.config.ReaderConfig;
import com.apache.flink.mysql.MySqlDatabaseMeta;
import com.apache.flink.rdb.DataSource;
import com.apache.flink.rdb.datareader.DistributedJdbcDataReader;
import com.apache.flink.rdb.inputformat.DistributedJdbcInputFormat;
import com.apache.flink.rdb.inputformat.DistributedJdbcInputFormatBuilder;
import com.apache.flink.rdb.util.DbUtil;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @Description: MysqldReader
 * <p>
 * @author: 894941211
 * <p>
 * @date: 2020/10/23
 */
public class MysqldReader extends DistributedJdbcDataReader {

    public MysqldReader(DataTransferConfig config, StreamExecutionEnvironment env) {
        super(config, env);
        setDatabaseInterface(new MySqlDatabaseMeta());
    }

    @Override
    protected DistributedJdbcInputFormatBuilder getBuilder(){
        return new DistributedJdbcInputFormatBuilder(new DistributedJdbcInputFormat());
    }

    @Override
    protected ArrayList<DataSource> buildConnections(){
        ArrayList<DataSource> sourceList = new ArrayList<>(connectionConfigs.size());
        for (ReaderConfig.ParameterConfig.ConnectionConfig connectionConfig : connectionConfigs) {
            String curUsername = (connectionConfig.getUsername() == null || connectionConfig.getUsername().length() == 0)
                    ? username : connectionConfig.getUsername();
            String curPassword = (connectionConfig.getPassword() == null || connectionConfig.getPassword().length() == 0)
                    ? password : connectionConfig.getPassword();
            String curJdbcUrl = DbUtil.formatJdbcUrl(connectionConfig.getJdbcUrl().get(0), Collections.singletonMap("zeroDateTimeBehavior", "convertToNull"));
            for (String table : connectionConfig.getTable()) {
                DataSource source = new DataSource();
                source.setTable(table);
                source.setUserName(curUsername);
                source.setPassword(curPassword);
                source.setJdbcUrl(curJdbcUrl);

                sourceList.add(source);
            }
        }

        return sourceList;
    }
}
